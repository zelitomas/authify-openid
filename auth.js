
class CredentialLoader {
    static getAccessToken(){
        return localStorage.getItem("access_token");
    }

    static getRefreshToken() {
        return localStorage.getItem("refresh_token");
    }

    static getIDToken() {
        return localStorage.getItem("id_token");
    }

    static setAccessToken(access_token) {
        CredentialLoader.setLocalStorage("access_token", access_token);
    }

    static setRefreshToken(refresh_token) {
        CredentialLoader.setLocalStorage("refresh_token", refresh_token);
    }

    static setIDToken(id_token) {
        CredentialLoader.setLocalStorage("id_token", id_token);
    }

    static setLocalStorage(key, value) {
        if(value === null) {
            localStorage.removeItem(key);
            return
        }
        localStorage.setItem(key, value);
    }

    static generateVerifierChallenge() {
        return crypto.randomBytes(32).toString('hex');
    }

    static getVerifierToken() {
        let token = localStorage.getItem("verifier_token");
        if(!token){
            token = CredentialLoader.generateVerifierChallenge();
            localStorage.setItem("verifier_token", token);
        }
        return token;
    }
}

class StateSaver {
    static saveState(state, text){
        localStorage.setItem("state_" + state, text);
    }

    static popState(state) {
        let item = localStorage.getItem("state_" + state);
        localStorage.removeItem("state_" + state);
        return item;
    }
}

class AuthenticationException {
    constructor(message) {
        this.message = message;
        this.name = 'AuthenticationException';
    }
}


class Authenticator{
    // Hardcoded for now

    constructor(options) {
        if (!options?.tokenEndpoint || !options?.authorizationEndpoint){
            throw "At least tokenEndpoint and authorizationEndpoint needs to be configured"
        }
        this.options = options
    }

    authenticatedFetch = async (resource, init) => {
        init = this.injectBearer(init);
        init.mode = 'cors';
        let response = await fetch(resource, init);
        if(response.status === 403 || response.status === 401){
            // try refreshing tokens, if it didn't help, just return error.
            try {
                await this.refreshTokens();
            } catch (e) {
                if (e instanceof AuthenticationException) {
                    this.logout(false)
                } else {
                    throw e
                }
            }

            init = this.injectBearer(init);
            response = await fetch(resource, init);
        }

        return response;
    };

    injectBearer(init){
        if (!init) {
            init = {}
        }
        let accessToken = CredentialLoader.getAccessToken();
        if(!accessToken) {
            return init;
        }
        if (!init.headers) {
            init.headers = {};
        }
        init.headers['Authorization'] = "Bearer " + accessToken;

        return init;
    }

    refreshTokens = async () => {
        let refreshToken = CredentialLoader.getRefreshToken()
        if(refreshToken === null || refreshToken === "") {
            return
        }
        let response = await fetch(this.options.tokenEndpoint, {
            body: new URLSearchParams({
                'client_id': this.options.clientId,
                'refresh_token': CredentialLoader.getRefreshToken(),
                'grant_type': 'refresh_token'
            }),
            headers: {
                "Content-Type": "application/x-www-form-urlencoded"
            },
            method: "POST"
        });

        await this.handleCodeResponse(response);
    };


    getRedirectUri = () => {
        return this.options.redirectUri ?? window.location;
    };

    getPostLogoutUri = () => {
        return this.options.logoutRedirectUri ?? window.location;
    };

    createState = () => {
        let state = Math.floor(Math.random() * 1000000);
        StateSaver.saveState(state, this.getRedirectUri());
        return state
    };

    initiateLogin = () => {
        let params = new URLSearchParams({
            'client_id': this.options.clientId,
            'redirect_uri': this.getRedirectUri(),
            'response_type': 'code',
            'state': this.createState(),
            'scope': this.options?.scope ?? "openid"
        });
        window.location = this.options.authorizationEndpoint + "?" + params
    };

    initiateEndSession = (id_token = null) => {
        let params = new URLSearchParams({
            'post_logout_redirect_uri': this.getPostLogoutUri(),
        });

        if(id_token !== null) {
            params.set("id_token_hint", id_token);
        }

        window.location = this.options.endSessionEndpoint + "?" + params
    };

    checkLogin = async () => {
        let params = new URLSearchParams(window.location.search);
        if(params.has('code')){
            let code_response = await fetch(this.options.tokenEndpoint, {
                body: new URLSearchParams({
                    'client_id': this.options.clientId,
                    'code': params.get('code'),
                    'grant_type': 'authorization_code',
                    'redirect_uri': StateSaver.popState(params.get("state"))
                }),
                headers: {
                    "Content-Type": "application/x-www-form-urlencoded"
                },
                method: "POST"
            });
            let result = await this.handleCodeResponse(code_response);
            this.removeAuthUrlParameters();
            let success = {success: true};
            return {...success, ...result};
        } else {
            return {success: false}
        }
    };

    handleCodeResponse = async (response) => {
        if(response.ok){
            let data = await response.json();
            CredentialLoader.setAccessToken(data.access_token);
            CredentialLoader.setRefreshToken(data.refresh_token);
            if(data.id_token) {
                CredentialLoader.setIDToken(data.id_token);
            }
            return {accessToken: data.access_token, refreshToken: data.refresh_token};
        } else {
            let error = await response.text();
            console.error(error);
            throw new AuthenticationException(error);
        }
    };

    logout = async (endSessionOnServer = true) => {
        if(!CredentialLoader.getAccessToken()) {
            console.warn("No session was ended because there was no session to end.");
            return;
        }

        let idToken = CredentialLoader.getIDToken();

        CredentialLoader.setIDToken(null);
        CredentialLoader.setRefreshToken(null);
        CredentialLoader.setAccessToken(null);

        if(endSessionOnServer) {
            if (this.options.endSessionEndpoint) {
                this.initiateEndSession(idToken);
            } else {
                console.warn("Session was ended only locally, because no endSessionEndpoint is configured!")
            }
        }
    };

    removeAuthUrlParameters = () => {
        let url = new URL(window.location);
        url.searchParams.delete("state");
        url.searchParams.delete("code");
        history.replaceState(null, '', url.href);

    }
}

export default Authenticator;
export {AuthenticationException};